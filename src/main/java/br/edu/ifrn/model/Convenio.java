package br.edu.ifrn.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.br.CNPJ;

@Entity
public class Convenio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2458821216966922685L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	private String registroANS;
	private String RazaoSocial;
	@CNPJ
	private String cnpj;
	private String codigoCNS;
	private String telefone;
	private String email;
	private int periodoRetorno;
	private Boolean particular;
	
	@OneToOne
	private Paciente paciente;

	@OneToOne
	private AtendimentoAgendado atendimentoAgendado; 
	
	@ManyToMany
	private List<Profissional> profissionais;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRegistroANS() {
		return registroANS;
	}

	public void setRegistroANS(String registroANS) {
		this.registroANS = registroANS;
	}

	public String getRazaoSocial() {
		return RazaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		RazaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCodigoCNS() {
		return codigoCNS;
	}

	public void setCodigoCNS(String codigoCNS) {
		this.codigoCNS = codigoCNS;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPeriodoRetorno() {
		return periodoRetorno;
	}

	public void setPeriodoRetorno(int periodoRetorno) {
		this.periodoRetorno = periodoRetorno;
	}

	public Boolean getParticular() {
		return particular;
	}

	public void setParticular(Boolean particular) {
		this.particular = particular;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public AtendimentoAgendado getAtendimentoAgendado() {
		return atendimentoAgendado;
	}

	public void setAtendimentoAgendado(AtendimentoAgendado atendimentoAgendado) {
		this.atendimentoAgendado = atendimentoAgendado;
	}

	public List<Profissional> getProfissionais() {
		return profissionais;
	}

	public void setProfissionais(List<Profissional> profissionais) {
		this.profissionais = profissionais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Convenio other = (Convenio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
