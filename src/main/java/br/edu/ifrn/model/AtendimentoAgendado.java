package br.edu.ifrn.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class AtendimentoAgendado implements Serializable {

	private static final long serialVersionUID = 1220342548275355142L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private int status;
	
	@Temporal(TemporalType.TIME)
	private Date horario;
	
	private Boolean retorno;
	private Double valorConsulta;
	private String nomeTemporario;
	private String contatoTemporario;
	
	@ManyToOne
	private RegrasAgenda regrasAgenda;
	
	@ManyToOne
	private Paciente paciente;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getHorario() {
		return horario;
	}

	public void setHorario(Date horario) {
		this.horario = horario;
	}

	public Boolean getRetorno() {
		return retorno;
	}

	public void setRetorno(Boolean retorno) {
		this.retorno = retorno;
	}

	public Double getValorConsulta() {
		return valorConsulta;
	}

	public void setValorConsulta(Double valorConsulta) {
		this.valorConsulta = valorConsulta;
	}

	public String getNomeTemporario() {
		return nomeTemporario;
	}

	public void setNomeTemporario(String nomeTemporario) {
		this.nomeTemporario = nomeTemporario;
	}

	public String getContatoTemporario() {
		return contatoTemporario;
	}

	public void setContatoTemporario(String contatoTemporario) {
		this.contatoTemporario = contatoTemporario;
	}

	public RegrasAgenda getRegrasAgenda() {
		return regrasAgenda;
	}

	public void setRegrasAgenda(RegrasAgenda regrasAgenda) {
		this.regrasAgenda = regrasAgenda;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtendimentoAgendado other = (AtendimentoAgendado) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
