/**
 * 
 */
package br.edu.ifrn.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Carlos
 *
 */
@Entity
public class RegrasAgenda implements Serializable {

	private static final long serialVersionUID = -6996493163536829028L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private int numeroPrimeiraVez;
	private int numeroConvenio;
	private int numeroRetorno;
	
	@Temporal(TemporalType.DATE)
	private Date dia;
	
	@Temporal(TemporalType.TIME)
	private Date horaInicio;
	
	@Temporal(TemporalType.TIME)
	private Date horaFim;
	
	private Double valorConsulta;
	
	private int tempoMedioConsulta;
	
	@Enumerated(EnumType.STRING)
	private TipoAtendimento tipoAtendimento;
	
	@ManyToOne
	private Profissional profissionais;
	
	@OneToMany
	private List<AtendimentoAgendado> atendimentoAgendados;

	public List<AtendimentoAgendado> getAtendimentoAgendados() {
		return atendimentoAgendados;
	}

	public void setAtendimentoAgendados(List<AtendimentoAgendado> atendimentoAgendados) {
		this.atendimentoAgendados = atendimentoAgendados;
	}

	public int getNumeroPrimeiraVez() {
		return numeroPrimeiraVez;
	}

	public void setNumeroPrimeiraVez(int numeroPrimeiraVez) {
		this.numeroPrimeiraVez = numeroPrimeiraVez;
	}

	public int getNumeroConvenio() {
		return numeroConvenio;
	}

	public void setNumeroConvenio(int numeroConvenio) {
		this.numeroConvenio = numeroConvenio;
	}

	public int getNumeroRetorno() {
		return numeroRetorno;
	}

	public void setNumeroRetorno(int numeroRetorno) {
		this.numeroRetorno = numeroRetorno;
	}

	public Date getDia() {
		return dia;
	}

	public void setDia(Date dia) {
		this.dia = dia;
	}

	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Date getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(Date horaFim) {
		this.horaFim = horaFim;
	}

	public Double getValorConsulta() {
		return valorConsulta;
	}

	public void setValorConsulta(Double valorConsulta) {
		this.valorConsulta = valorConsulta;
	}

	public int getTempoMedioConsulta() {
		return tempoMedioConsulta;
	}

	public void setTempoMedioConsulta(int tempoMedioConsulta) {
		this.tempoMedioConsulta = tempoMedioConsulta;
	}

	public TipoAtendimento getTipoAtendimento() {
		return tipoAtendimento;
	}

	public void setTipoAtendimento(TipoAtendimento tipoAtendimento) {
		this.tipoAtendimento = tipoAtendimento;
	}


	public Profissional getProfissionais() {
		return profissionais;
	}

	public void setProfissionais(Profissional profissionais) {
		this.profissionais = profissionais;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegrasAgenda other = (RegrasAgenda) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
