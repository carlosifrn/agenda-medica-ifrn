package br.edu.ifrn.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import br.edu.ifrn.model.Especialidade;

@Entity
@PrimaryKeyJoinColumn(name="id")
public class Profissional extends Pessoa implements Serializable {

	private static final long serialVersionUID = -8146001271265004383L;
	
	private String conselhoProfissional;
	private String estado;
	private String numeroConselho;
	private String nqe;
	private String descricaoAPP;
	private Boolean isAPP;
		
	@ManyToOne
	private TipoProfissional tipoProfissional;
	@ManyToMany
	private List<Convenio> convenios;
	@ManyToMany
	private List<LocalAtendimento> locaisAtendimentos;
	@ManyToMany
    private List<Especialidade> especialidades;
	
	
	
	public TipoProfissional getTipoProfissional() {
		return tipoProfissional;
	}

	public void setTipoProfissional(TipoProfissional tipoProfissional) {
		this.tipoProfissional = tipoProfissional;
	}

	public String getConselhoProfissional() {
		return conselhoProfissional;
	}

	public void setConselhoProfissional(String conselhoProfissional) {
		this.conselhoProfissional = conselhoProfissional;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNumeroConselho() {
		return numeroConselho;
	}

	public void setNumeroConselho(String numeroConselho) {
		this.numeroConselho = numeroConselho;
	}

	public String getNqe() {
		return nqe;
	}

	public void setNqe(String nqe) {
		this.nqe = nqe;
	}

	public String getDescricaoAPP() {
		return descricaoAPP;
	}

	public void setDescricaoAPP(String descricaoAPP) {
		this.descricaoAPP = descricaoAPP;
	}

	public Boolean getIsAPP() {
		return isAPP;
	}

	public void setIsAPP(Boolean isAPP) {
		this.isAPP = isAPP;
	}

	public List<Especialidade> getEspecialidades() {
        return especialidades;
    }

    public void setEspecialidades(List<Especialidade> especialidades) {
        this.especialidades = especialidades;
    }

	public List<Convenio> getConvenios() {
		return convenios;
	}

	public void setConvenios(List<Convenio> convenios) {
		this.convenios = convenios;
	}

	public List<LocalAtendimento> getLocaisAtendimentos() {
		return locaisAtendimentos;
	}

	public void setLocaisAtendimentos(List<LocalAtendimento> locaisAtendimentos) {
		this.locaisAtendimentos = locaisAtendimentos;
	}

	
}
