package br.edu.ifrn.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="id")
public class Paciente extends Pessoa implements Serializable {

	private static final long serialVersionUID = -9211637169841284271L;

	public Paciente() {
	
	}
	public Paciente(Long id) {
		setId(id);
	}
	
	private HistoricoPacienteAPP historicoPacienteAPP;
	
	@ManyToOne
	private Convenio convenio;
	
	
	public Convenio getConvenio() {
		return convenio;
	}
	public void setConvenio(Convenio convenio) {
		this.convenio = convenio;
	}
	public HistoricoPacienteAPP getHistoricoPacienteAPP() {
		return historicoPacienteAPP;
	}
	public void setHistoricoPacienteAPP(HistoricoPacienteAPP historicoPacienteAPP) {
		this.historicoPacienteAPP = historicoPacienteAPP;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((historicoPacienteAPP == null) ? 0 : historicoPacienteAPP.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paciente other = (Paciente) obj;
		if (historicoPacienteAPP == null) {
			if (other.historicoPacienteAPP != null)
				return false;
		} else if (!historicoPacienteAPP.equals(other.historicoPacienteAPP))
			return false;
		return true;
	} 
	
}
