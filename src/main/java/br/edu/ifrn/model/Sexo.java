package br.edu.ifrn.model;

public enum Sexo {
	MASCULINO,
	FEMININO;
}
