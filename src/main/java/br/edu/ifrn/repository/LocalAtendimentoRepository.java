package br.edu.ifrn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.ifrn.model.LocalAtendimento;

@Repository
public interface LocalAtendimentoRepository extends JpaRepository<LocalAtendimento, Long> {
	@Query("select c from LocalAtendimento c where c.id = :id")
    public LocalAtendimento findLocalAtendimentoById(@Param("id") Long id);
}
