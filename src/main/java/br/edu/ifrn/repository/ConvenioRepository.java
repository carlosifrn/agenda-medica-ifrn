package br.edu.ifrn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.ifrn.model.Convenio;

@Repository
public interface ConvenioRepository extends JpaRepository<Convenio, Long> {
	@Query("select c from Convenio c where c.id = :id")
    public Convenio findConvenioById(@Param("id") Long id);
}
