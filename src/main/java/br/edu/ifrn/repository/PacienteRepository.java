package br.edu.ifrn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.ifrn.model.Paciente;

@Repository
public interface PacienteRepository extends JpaRepository<Paciente, Long> {
	@Query("select c from Paciente c where c.id = :id")
    public Paciente findPacienteById(@Param("id") Long id);
}
