package br.edu.ifrn.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifrn.model.LocalAtendimento;
import br.edu.ifrn.repository.LocalAtendimentoRepository;

@Controller
@RequestMapping
public class LocalAtendimentoController {

	@Autowired
	private LocalAtendimentoRepository localAtendimentoRepository;

	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping("/localAtendimento")
	public ModelAndView exibirLocalAtendimento(LocalAtendimento localAtendimento) {
		ModelAndView modelAndView = new ModelAndView("localAtendimento");
		if (localAtendimento == null)
			modelAndView.addObject("localAtendimento", new LocalAtendimento());
		else
			modelAndView.addObject("localAtendimento", localAtendimento);
		modelAndView.addObject("localAtendimento", localAtendimentoRepository.findAll());
		return modelAndView;
	}

	@PostMapping
	public ModelAndView salvarLocalAtendimento(@Valid LocalAtendimento localAtendimento, BindingResult resultado) {
		if (resultado.hasErrors()) {
			return exibirLocalAtendimento(localAtendimento);
		}
		localAtendimentoRepository.save(localAtendimento);
		return new ModelAndView("redirect:/clientes");
	}

	@GetMapping("/excluir/{id}")
	public ModelAndView excluirLocalAtendimento(@PathVariable("id") Long id) {
		LocalAtendimento localAtendimento = localAtendimentoRepository.findLocalAtendimentoById(id);
		localAtendimentoRepository.delete(localAtendimento);
		return exibirLocalAtendimento(null);
	}

	@GetMapping("/editar/{id}")
	public ModelAndView editarLocalAtendimento(@PathVariable("id") Long id) {
		LocalAtendimento localAtendimento = localAtendimentoRepository.findLocalAtendimentoById(id);
		return exibirLocalAtendimento(localAtendimento);
	}
}
