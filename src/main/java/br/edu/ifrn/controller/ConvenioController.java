package br.edu.ifrn.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifrn.model.Convenio;
import br.edu.ifrn.repository.ConvenioRepository;

@Controller
@RequestMapping
public class ConvenioController {
	
	@Autowired
	private ConvenioRepository convenioRepository;

	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping("/convenio")
	public ModelAndView exibirConvenio(Convenio convenio) {
		ModelAndView modelAndView = new ModelAndView("convenio");
		if (convenio == null)
			modelAndView.addObject("convenio", new Convenio());
		else
			modelAndView.addObject("convenio", convenio);
		modelAndView.addObject("convenio", convenioRepository.findAll());
		return modelAndView;
	}

	@PostMapping
	public ModelAndView salvarConvenio(@Valid Convenio convenio, BindingResult resultado) {
		if (resultado.hasErrors()) {
			return exibirConvenio(convenio);
		}
		convenioRepository.save(convenio);
		return new ModelAndView("redirect:/clientes");
	}

	@GetMapping("/excluir/{id}")
	public ModelAndView excluirConvenio(@PathVariable("id") Long id) {
		Convenio convenio = convenioRepository.findConvenioById(id);
		convenioRepository.delete(convenio);
		return exibirConvenio(null);
	}

	@GetMapping("/editar/{id}")
	public ModelAndView editarConvenio(@PathVariable("id") Long id) {
		Convenio convenio = convenioRepository.findConvenioById(id);
		return exibirConvenio(convenio);
	}
}
