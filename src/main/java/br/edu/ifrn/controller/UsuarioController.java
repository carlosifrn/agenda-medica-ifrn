package br.edu.ifrn.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifrn.model.Usuario;
import br.edu.ifrn.repository.UsuarioRepository;

@Controller
@RequestMapping
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping("/usuario")
	public ModelAndView exibirUsuario(Usuario usuario) {
		ModelAndView modelAndView = new ModelAndView("usuario");
		if (usuario == null)
			modelAndView.addObject("usuario", new Usuario());
		else
			modelAndView.addObject("usuario", usuario);
		modelAndView.addObject("usuario", usuarioRepository.findAll());
		return modelAndView;
	}

	@PostMapping
	public ModelAndView salvarUsuario(@Valid Usuario usuario, BindingResult resultado) {
		if (resultado.hasErrors()) {
			return exibirUsuario(usuario);
		}
		usuarioRepository.save(usuario);
		return new ModelAndView("redirect:/clientes");
	}

	@GetMapping("/excluir/{id}")
	public ModelAndView excluirUsuario(@PathVariable("id") Long id) {
		Usuario usuario = usuarioRepository.findUsuarioById(id);
		usuarioRepository.delete(usuario);
		return exibirUsuario(null);
	}

	@GetMapping("/editar/{id}")
	public ModelAndView editarUsuario(@PathVariable("id") Long id) {
		Usuario usuario = usuarioRepository.findUsuarioById(id);
		return exibirUsuario(usuario);
	}
}
