package br.edu.ifrn.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifrn.model.Profissional;
import br.edu.ifrn.repository.ProfissionalRepository;

@Controller
@RequestMapping
public class ProfissionalController {

	@Autowired
	private ProfissionalRepository profissionalRepository;

	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping("/profissional")
	public ModelAndView exibirProfissional(Profissional profissional) {
		ModelAndView modelAndView = new ModelAndView("profissional");
		if (profissional == null)
			modelAndView.addObject("profissional", new Profissional());
		else
			modelAndView.addObject("profissional", profissional);
		modelAndView.addObject("profissional", profissionalRepository.findAll());
		return modelAndView;
	}

	@PostMapping
	public ModelAndView salvarProfissional(@Valid Profissional profissional, BindingResult resultado) {
		if (resultado.hasErrors()) {
			return exibirProfissional(profissional);
		}
		profissionalRepository.save(profissional);
		return new ModelAndView("redirect:/clientes");
	}

	@GetMapping("/excluir/{id}")
	public ModelAndView excluirProfissional(@PathVariable("id") Long id) {
		Profissional profissional = profissionalRepository.findProfissionalById(id);
		profissionalRepository.delete(profissional);
		return exibirProfissional(null);
	}

	@GetMapping("/editar/{id}")
	public ModelAndView editarProfissional(@PathVariable("id") Long id) {
		Profissional profissional = profissionalRepository.findProfissionalById(id);
		return exibirProfissional(profissional);
	}
}
