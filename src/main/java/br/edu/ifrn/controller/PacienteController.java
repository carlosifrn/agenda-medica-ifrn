package br.edu.ifrn.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifrn.model.Paciente;
import br.edu.ifrn.repository.PacienteRepository;

@Controller
@RequestMapping
public class PacienteController {
	@Autowired
	private PacienteRepository pacienteRepository;

	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping("/paciente")
	public ModelAndView exibirPaciente(Paciente paciente) {
		ModelAndView modelAndView = new ModelAndView("paciente");
		if (paciente == null)
			modelAndView.addObject("paciente", new Paciente());
		else
			modelAndView.addObject("paciente", paciente);
		modelAndView.addObject("paciente", pacienteRepository.findAll());
		return modelAndView;
	}

	@PostMapping
	public ModelAndView salvarPaciente(@Valid Paciente paciente, BindingResult resultado) {
		if (resultado.hasErrors()) {
			return exibirPaciente(paciente);
		}
		pacienteRepository.save(paciente);
		return new ModelAndView("redirect:/clientes");
	}

	@GetMapping("/excluir/{id}")
	public ModelAndView excluirPaciente(@PathVariable("id") Long id) {
		Paciente paciente = pacienteRepository.findPacienteById(id);
		pacienteRepository.delete(paciente);
		return exibirPaciente(null);
	}

	@GetMapping("/editar/{id}")
	public ModelAndView editarPaciente(@PathVariable("id") Long id) {
		Paciente paciente = pacienteRepository.findPacienteById(id);
		return exibirPaciente(paciente);
	}
}
